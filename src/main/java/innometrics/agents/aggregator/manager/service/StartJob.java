package innometrics.agents.aggregator.manager.service;

import innometrics.agents.aggregator.manager.agents.AgentService;
//import innometrics.agents.aggregator.manager.agents.gitlabV1.service.GitLabService;
import innometrics.agents.aggregator.manager.agents.gitlabV2.GitlabV2Service;
import innometrics.agents.aggregator.manager.agents.gitlabV2.feign.GitlabAPIFeignClient;
import innometrics.agents.aggregator.manager.model.AgentRegistry;
import innometrics.agents.aggregator.manager.repository.AgentRegistryRepository;
import innometrics.agents.aggregator.manager.repository.AgentXRepoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StartJob implements ApplicationRunner {


    @Autowired
    AgentRegistryRepository agentRegistryRepository;

    @Autowired
    AgentXRepoRepository agentXRepoRepository;

    @Autowired
    AggregatorFeignClient aggregatorFeignClient;

    @Autowired
    GitlabAPIFeignClient gitlabAPIFeignClient;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        List<AgentRegistry> registries = agentRegistryRepository.findAll();

        for (AgentRegistry registry : registries) {
            AgentService service = getAgentService(registry);
            service.startFetching(registry);
        }

    }

    private AgentService getAgentService(AgentRegistry registry) {
        AgentService service;

        if (registry.getUid() == 1) {
            service = new GitlabV2Service(gitlabAPIFeignClient, agentXRepoRepository, aggregatorFeignClient);
        } else {
            service = new GitlabV2Service(gitlabAPIFeignClient, agentXRepoRepository, aggregatorFeignClient);

        }

        return service;

    }
}
