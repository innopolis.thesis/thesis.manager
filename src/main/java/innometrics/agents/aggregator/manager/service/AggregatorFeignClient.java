package innometrics.agents.aggregator.manager.service;

import com.fasterxml.jackson.databind.JsonNode;
import innometrics.agents.aggregator.manager.model.Metric;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(
        name = "aggregator",
        url = "${agents.aggregator}"
)
public interface AggregatorFeignClient {

    @PostMapping("metric")
    void saveMetrics(@RequestBody List<Metric> metrics);
}
