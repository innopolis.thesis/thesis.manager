package innometrics.agents.aggregator.manager.agents.gitlabV2.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Event {

    private Long id;

    @JsonProperty("action_name")
    private String actionName;

    @JsonProperty("target_id")
    private String targetId;

    @JsonProperty("target_type")
    private String targetType;

    @JsonProperty("author_id")
    private String authorId;

    @JsonProperty("target_title")
    private String targetTitle;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("project_id")
    private long projectId;
}
