package innometrics.agents.aggregator.manager.agents.gitlabV2;

import feign.Response;
import innometrics.agents.aggregator.manager.agents.AgentService;
import innometrics.agents.aggregator.manager.agents.gitlabV2.dto.Commit;
import innometrics.agents.aggregator.manager.agents.gitlabV2.dto.Event;
import innometrics.agents.aggregator.manager.agents.gitlabV2.dto.Issue;
import innometrics.agents.aggregator.manager.agents.gitlabV2.dto.Project;
import innometrics.agents.aggregator.manager.agents.gitlabV2.feign.GitlabAPIFeignClient;
import innometrics.agents.aggregator.manager.model.*;
import innometrics.agents.aggregator.manager.repository.AgentRegistryRepository;
import innometrics.agents.aggregator.manager.repository.AgentXRepoRepository;
import innometrics.agents.aggregator.manager.service.AggregatorFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class GitlabV2Service implements AgentService {

    GitlabAPIFeignClient gitlabAPIFeignClient;

    AgentXRepoRepository agentXRepoRepository;

    AggregatorFeignClient aggregatorFeignClient;

    public GitlabV2Service(GitlabAPIFeignClient gitlabAPIFeignClient, AgentXRepoRepository agentXRepoRepository, AggregatorFeignClient aggregatorFeignClient) {
        this.gitlabAPIFeignClient = gitlabAPIFeignClient;
        this.agentXRepoRepository = agentXRepoRepository;
        this.aggregatorFeignClient = aggregatorFeignClient;
    }

    @Override
    public void startFetching(AgentRegistry registry) {
        List<AgentXRepo> registryItems = agentXRepoRepository.findAllByUidRegistry(registry.getUid());


        registryItems.parallelStream().forEach(registryitem -> {

            List<Project> projects = new ArrayList<>();

            for (int page = 1; page <= 1; page++) {

                projects.addAll(gitlabAPIFeignClient.getAllProjects(
                        registryitem.getMetaData("private_token"),
                        new HashMap<>() {{
                            put("membership", "true");
                            put("per_page", "100");
                        }}
                ));
            }


            log.info("start fetching for projects:\n" + projects.stream().map(p -> p.getId() + "l").collect(Collectors.joining(",")));


            for (Project project : projects) {

                log.info("doing now {}",project);

                List<Commit> commits = getAllPagesOf(Commit.class, project, registryitem);

                List<Issue> issues = getAllPagesOf(Issue.class, project, registryitem);

                List<Event> events = getAllPagesOf(Event.class, project, registryitem);

                try {
                    sendEventsToAggregator(events,project.getId(),aggregatorFeignClient);
                    sendIssuesToAggregator(issues,project.getId(),aggregatorFeignClient);
                    sendCommitsToAggregator(commits,project.getId(),aggregatorFeignClient);
                }catch (Exception e){
                    e.printStackTrace();
                    log.error("error during send metrics");
                }
            }

        });

    }

    public void sendCommitsToAggregator(List<Commit> commits, Long projectId,  AggregatorFeignClient aggregatorFeignClient) {
        aggregatorFeignClient.saveMetrics(commits.stream().map(el -> Metric.of(el,projectId)).collect(Collectors.toList()));
    }
    public void sendIssuesToAggregator(List<Issue> issues, Long projectId, AggregatorFeignClient aggregatorFeignClient) {
        aggregatorFeignClient.saveMetrics(issues.stream().map(el -> Metric.of(el,projectId)).collect(Collectors.toList()));
    }
    public void sendEventsToAggregator(List<Event> events, Long projectId,  AggregatorFeignClient aggregatorFeignClient) {
        aggregatorFeignClient.saveMetrics(events.stream().map(el -> Metric.of(el,projectId)).collect(Collectors.toList()));
    }

    public List getAllPagesOf(Class clz, Project project, AgentXRepo registryItem) {

        try {
            if (clz.equals(Commit.class)) {

                List<Commit> commits = new ArrayList<>();

                int page = 1;
                Integer lastPage = null;

                do {
                    ResponseEntity<List<Commit>> response = gitlabAPIFeignClient.getCommits(
                            registryItem.getMetaData("private_token"),
                            new HashMap<>() {{
                                put("per_page", "100");
                                put("page", String.valueOf(page));
                            }},
                            project.getId()
                    );

                    commits.addAll(response.getBody());

                    if (lastPage == null) {
                        lastPage = getNumberOfPages(response.getHeaders().get("link").get(0));
                    }


                } while (page != lastPage);

                return commits;
            } else if (clz.equals(Event.class)) {

                List<Event> events = new ArrayList<>();

                int page = 1;
                Integer lastPage = null;

                do {
                    ResponseEntity<List<Event>> response = gitlabAPIFeignClient.getEvents(
                            registryItem.getMetaData("private_token"),
                            new HashMap<>() {{
                                put("per_page", "100");
                                put("page", String.valueOf(page));
                            }},
                            project.getId()
                    );

                    events.addAll(response.getBody());

                    if (lastPage == null) {
                        lastPage = getNumberOfPages(response.getHeaders().get("link").get(0));
                    }


                } while (page != lastPage);

                return events;
            } else if (clz.equals(Issue.class)) {

                List<Issue> issues = new ArrayList<>();

                int page = 1;
                Integer lastPage = null;

                do {
                    ResponseEntity<List<Issue>> response = gitlabAPIFeignClient.getIssues(
                            registryItem.getMetaData("private_token"),
                            new HashMap<>() {{
                                put("per_page", "100");
                                put("page", String.valueOf(page));
                            }},
                            project.getId()
                    );

                    issues.addAll(response.getBody());

                    if (lastPage == null) {
                        lastPage = getNumberOfPages(response.getHeaders().get("link").get(0));
                    }


                } while (page != lastPage);

                return issues;
            } else {
                return Collections.emptyList();
            }
        } catch (Exception e) {
            return Collections.emptyList();
        }


    }


    public int getNumberOfPages(String links) {
        for (String el : links.split(",")) {
            String link = el.split(";")[0];
            String rel = el.split(";")[1];

            if (rel.contains("last")) {
                return Integer.parseInt(Arrays.stream(link.split("&")).filter(s -> s.contains("page=")).findFirst().get().split("=")[1]);
            }
        }

        return 1;
    }
}
