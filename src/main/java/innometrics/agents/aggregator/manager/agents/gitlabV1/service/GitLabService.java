//package innometrics.agents.aggregator.manager.agents.gitlabV1.service;
//
//
//import feign.Feign;
//import feign.Logger;
//import feign.gson.GsonDecoder;
//import feign.gson.GsonEncoder;
//import feign.okhttp.OkHttpClient;
//import feign.slf4j.Slf4jLogger;
//import innometrics.agents.aggregator.manager.agents.AgentService;
//import innometrics.agents.aggregator.manager.agents.gitlabV1.feign.GitlabFeignClient;
//import innometrics.agents.aggregator.manager.agents.gitlabV1.model.Commit;
//import innometrics.agents.aggregator.manager.agents.gitlabV1.model.Event;
//import innometrics.agents.aggregator.manager.agents.gitlabV1.model.Issue;
//import innometrics.agents.aggregator.manager.model.AgentRegistry;
//import innometrics.agents.aggregator.manager.repository.GitlabV1RegistryItemRepository;
//import innometrics.agents.aggregator.manager.service.AggregatorFeignClient;
//import lombok.AccessLevel;
//import lombok.experimental.FieldDefaults;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.cloud.openfeign.support.SpringMvcContract;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//@Slf4j
//@FieldDefaults(
//        level = AccessLevel.PRIVATE
//)
//
//public class GitLabService implements AgentService {
//
//    GitlabV1RegistryItemRepository gitlabV1RegistryItemRepository;
//
//    AggregatorFeignClient aggregatorFeignClient;
//
//
//    @Override
//    public void startFetching(AgentRegistry registry) {
//
//        List<GitlabV1RegistryItem> registryItems = gitlabV1RegistryItemRepository.findAllByUidRegistry(registry.getUid());
//
//        long startTime = System.nanoTime();
//
//        List<Commit> allCommits = new ArrayList<>();
//        List<Event> allEvents = new ArrayList<>();
//        List<Issue> allIssues = new ArrayList<>();
//
//        registryItems.parallelStream().forEach(registryItem -> {
//            String uri = "http://" + registryItem.getIp() + ":" + registryItem.getPort();
//
//            GitlabFeignClient gitlabFeignClient = Feign.builder()
//                    .client(new OkHttpClient())
//                    .encoder(new GsonEncoder())
//                    .decoder(new GsonDecoder())
//                    .logger(new Slf4jLogger(GitlabFeignClient.class))
//                    .logLevel(Logger.Level.FULL)
//                    .contract(new SpringMvcContract())
//                    .target(GitlabFeignClient.class, uri);
//
//
//            List<Long> myProjectUids = Arrays.asList(26888112l,26066474l,26066272l,25891796l,25343183l,25185431l,24697543l,23122777l,21088084l,20058668l,19015807l,18896756l,18479147l,17152085l,17107707l,17028562l,16388916l,15302971l,15141297l,15130435l,15125003l,15089337l,15078965l,15077904l,15067965l,14993197l,14958955l,11358814l);
////            List<String> myProjectCaption = Arrays.asList("thesis.aggregator", "thesis.manager", "my_awemose_project", "thesis.listener.webhook", "thesis.listener", "innometrics.thesis", "IPOD_refactor_project", "react_demo", "test_shop", "CuteBooker", "ncs_filan_integer_overflow_vuln", "innometrics.jenkins", "ncs_assignment", "ncs6", "gitlab.agent.spring", "Tinkoff_contest", "Distributed_file_system", "ML_project", "aliya", "DS_lab_9", "IT_week_11", "IT_ass_3", "FCS_ass_8", "FCS_ass_7", "DS_lab_8", "ML_HM_2", "Face", "Clinic Management System", "Attender");
//
//
//
//            List<Long> projectUids;
////            projectUids = Arrays.asList(26911227l,26911226l,26911215l,26911210l,26911207l,26911201l,26911195l,26911194l,26911190l,26911189l,26911185l,26911183l,26911181l,26911158l,26911155l,26911146l,26911141l,26911139l,26911138l,26911137l,26911128l,26911126l,26911122l,26911119l,26911117l,26911110l,26911105l,26911103l,26911102l,26911096l,26911081l,26911068l,26911058l,26911055l,26911045l,26911044l,26911039l,26911033l,26911027l,26911020l,26911014l,26911013l,26911011l,26911007l,26911001l,26910998l,26910991l,26910986l,26910982l,26910979l,26910975l,26910971l,26910967l,26910966l,26910965l,26910963l,26910964l,26910962l,26910961l,26910953l,26910950l,26910943l,26910941l,26910939l,26910936l,26910930l,26910922l,26910913l,26910909l,26910908l,26910904l,26910903l,26910899l,26910896l,26910895l,26910890l,26910889l,26910885l,26910884l,26910883l,26910882l,26910881l,26910880l,26910876l,26910875l,26910874l,26910871l,26910870l,26910868l,26910867l,26910864l,26910855l,26910854l,26910853l,26910850l,26910848l,26910843l,26910841l,26910838l,26910832l);
//            projectUids = myProjectUids;
//            log.info("start fetching for projects:\n" +projectUids);
//
////             send request ot fetch all the data
//            projectUids.stream().forEach(p -> {
//
//                try {
//                    gitlabFeignClient.fetchProject("aALxronbZxYEJFa7w27a", String.valueOf(p));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    System.out.println(p);
//                }
//            });
//
//
//
//
//            for (Long projectUid : projectUids) {
//                log.info("doing now {}",projectUid);
//                List<Commit> commits = gitlabFeignClient.getProjectCommits(projectUid);
//                List<Event> events = gitlabFeignClient.getProjectEvents(projectUid);
//                List<Issue> issues = gitlabFeignClient.getProjectIssues(projectUid);
//
//                allCommits.addAll(commits);
//                allEvents.addAll(events);
//                allIssues.addAll(issues);
//
//            }
//
//
////            aggregatorFeignClient.saveMetrics(commits,"Commit");
////            aggregatorFeignClient.saveMetrics(events,"Event");
////            aggregatorFeignClient.saveMetrics(issues,"Issue");
//
//        });
//        long stopTime = System.nanoTime();
//        System.out.println("v1\n" +
//                "time: " + (stopTime - startTime) / 1000000000 + " sec\n" +
//                "# commits, issues and events: " + allCommits.size() + ", " + allIssues.size() + ", " + allEvents.size());
//
//
//    }
//
//
//    public GitLabService(GitlabV1RegistryItemRepository gitlabV1RegistryItemRepository, AggregatorFeignClient aggregatorFeignClient) {
//        this.gitlabV1RegistryItemRepository = gitlabV1RegistryItemRepository;
//        this.aggregatorFeignClient = aggregatorFeignClient;
//    }
//}
