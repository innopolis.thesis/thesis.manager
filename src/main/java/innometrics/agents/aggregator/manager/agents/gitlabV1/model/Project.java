package innometrics.agents.aggregator.manager.agents.gitlabV1.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Project {

    private Long projectId;

    private String name;

    private String path;

    private String token;



}
