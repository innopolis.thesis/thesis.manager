package innometrics.agents.aggregator.manager.agents.gitlabV2.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Map;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(
        level = AccessLevel.PRIVATE
)
@ToString
public class Project {

    @JsonProperty("id")
    Long id;

    @JsonProperty("name")
    String name;

    @JsonProperty("_links")
    ProjectLinks links;



    @Data
    @RequiredArgsConstructor
    @FieldDefaults(
            level = AccessLevel.PRIVATE
    )
    public class ProjectLinks {

        @JsonProperty("issues")
        String issues;

        @JsonProperty("events")
        String events;
    }
}
