package innometrics.agents.aggregator.manager.agents.jenkins.feign;

import innometrics.agents.aggregator.manager.agents.jenkins.model.JobDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

//@FeignClient(
//        value = "agents-jenkins",
//        url = "${agents.jenkins}"
//)
public interface JenkinsFeignClient {


    @GetMapping
    public List<JobDTO> getJobs();

}
