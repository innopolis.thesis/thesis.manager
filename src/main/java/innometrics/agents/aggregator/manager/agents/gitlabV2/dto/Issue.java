package innometrics.agents.aggregator.manager.agents.gitlabV2.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Id;

@Data
@RequiredArgsConstructor
public class Issue {

    private Long id;


    @JsonProperty("description")
    private String description;

    @JsonProperty("state")
    private String state;

    @JsonProperty("title")
    private String title;

    @JsonProperty("updated_at")
    private String updatedAt;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("closed_at")
    private String closedAt;
}
