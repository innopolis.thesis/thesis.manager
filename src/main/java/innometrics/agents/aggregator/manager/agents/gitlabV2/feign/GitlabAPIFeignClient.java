package innometrics.agents.aggregator.manager.agents.gitlabV2.feign;

import feign.Response;
import innometrics.agents.aggregator.manager.agents.gitlabV2.dto.Commit;
import innometrics.agents.aggregator.manager.agents.gitlabV2.dto.Event;
import innometrics.agents.aggregator.manager.agents.gitlabV2.dto.Issue;
import innometrics.agents.aggregator.manager.agents.gitlabV2.dto.Project;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(
        url = "https://gitlab.com/api/v4/",
        value = "gitlabApi"
)
public interface GitlabAPIFeignClient {

    @GetMapping("projects")
    public List<Project> getAllProjects(
            @RequestParam(value = "private_token") String privateToken,
            @RequestParam Map<String,String> attributes
    );

    @GetMapping("projects/{project_id}/repository/commits")
    public ResponseEntity<List<Commit>> getCommits(
            @RequestParam(value = "private_token") String privateToken,
            @RequestParam Map<String,String> attributes,
            @PathVariable("project_id") Long projectId
    );

    @GetMapping("projects/{project_id}/issues")
    public ResponseEntity<List<Issue>> getIssues(
            @RequestParam(value = "private_token") String privateToken,
            @RequestParam Map<String,String> attributes,
            @PathVariable("project_id") Long projectId
    );

    @GetMapping("projects/{project_id}/events")
    public ResponseEntity<List<Event>> getEvents(
            @RequestParam(value = "private_token") String privateToken,
            @RequestParam Map<String,String> attributes,
            @PathVariable("project_id") Long projectId
    );


}
