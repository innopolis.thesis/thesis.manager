package innometrics.agents.aggregator.manager.agents.gitlabV2.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Data
@RequiredArgsConstructor
@FieldDefaults(
        level = AccessLevel.PRIVATE
)
@ToString
public class Commit {


    String id;

    @JsonProperty("author_name")
    private String authorName;

    @JsonProperty("committed_date")
    private String committedDate;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("title")
    private String title;

    @JsonProperty("message")
    private String message;
}
