package innometrics.agents.aggregator.manager.agents;

import innometrics.agents.aggregator.manager.model.AgentRegistry;

import java.util.List;

public interface AgentService {

    void startFetching(AgentRegistry registry); 

}
