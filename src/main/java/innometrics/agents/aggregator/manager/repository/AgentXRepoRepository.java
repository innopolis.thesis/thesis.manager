package innometrics.agents.aggregator.manager.repository;

import innometrics.agents.aggregator.manager.model.AgentXRepo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgentXRepoRepository extends JpaRepository<AgentXRepo,Long> {

    List<AgentXRepo> findAllByUidRegistry(Long uidRegistry);
}
