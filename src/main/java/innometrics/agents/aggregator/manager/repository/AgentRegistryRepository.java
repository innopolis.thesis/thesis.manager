package innometrics.agents.aggregator.manager.repository;

import innometrics.agents.aggregator.manager.model.AgentRegistry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgentRegistryRepository extends JpaRepository<AgentRegistry,Long> {
}
