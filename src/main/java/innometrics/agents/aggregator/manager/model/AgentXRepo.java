package innometrics.agents.aggregator.manager.model;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.annotation.Transient;

import javax.persistence.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Data
@Entity
@Table(name = "agent_x_repo")
@Slf4j
public class AgentXRepo {


    @Id
    Long uid;


    @Column(name = "uid_agent_registry")
    Long uidRegistry;

    @Column(name = "uid_repo")
    Long uidRepo;

    @Column(name = "meta_data")
    String metaData;


    public String getMetaData(String key) {

        Map<String, String> map;
        try {
            map = new ObjectMapper().readValue(this.metaData, HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("no meta data");
            map = new HashMap<>();
        }

        return map.getOrDefault(key, null);


    }


}
