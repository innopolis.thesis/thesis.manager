package innometrics.agents.aggregator.manager.model;

import lombok.Data;

import javax.persistence.*;

//@Table(name = "gitlab_registry_item")
//@Entity
@Data
public class GitlabV1RegistryItem {
//    @Id
//    @Column(name = "uid")
    private Long uid;

//    @Column(name = "uid_agent_registry")
    Long uidRegistry;

//    @Column(name = "ip")
    private String ip;

//    @Column(name = "port")
    private Long port;

//    @Column(name = "data")
    private String data;
}

