package innometrics.agents.aggregator.manager.model;

import innometrics.agents.aggregator.manager.agents.gitlabV2.dto.Commit;
import innometrics.agents.aggregator.manager.agents.gitlabV2.dto.Event;
import innometrics.agents.aggregator.manager.agents.gitlabV2.dto.Issue;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Metric {

    Long uid;

    String metricId;

    String author;

    Long projectId;

    Long agentId;

    Long metricType;

    String time;

    String meta;



    public static Metric of(Commit commit, Long projectId){
        return Metric.builder()
                .metricId(commit.getId())
                .author(commit.getAuthorName())
                .projectId(projectId)
                .agentId(1L)
                .metricType(1L) // TODO: 28.06.2021 define it
                .meta(commit.toString())
                .time(commit.getCreatedAt())
                .build();
    }

    public static Metric of(Event event, Long projectId){
        return Metric.builder()
                .metricId(String.valueOf(event.getId()))
                .author(event.getAuthorId())
                .projectId(projectId)
                .agentId(1L)
                .metricType(2L) // TODO: 28.06.2021 define it
                .meta(event.toString())
                .time(event.getCreatedAt())
                .build();
    }

    public static Metric of(Issue issue, Long projectId){
        return Metric.builder()
                .metricId(String.valueOf(issue.getId()))
                .author(null) // TODO: 28.06.2021 add
                .projectId(projectId)
                .agentId(1L)
                .metricType(1L) // TODO: 28.06.2021 define it
                .meta(issue.toString())
                .time(issue.getCreatedAt())
                .build();
    }




}
