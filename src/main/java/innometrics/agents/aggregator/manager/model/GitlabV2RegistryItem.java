package innometrics.agents.aggregator.manager.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "gitlab_agent_registry_item")
@Entity
@Data
public class GitlabV2RegistryItem {
    @Id
    @Column(name = "uid")
    private Long uid;

    @Column(name = "private_token")
    private String privateToken;
}

