package innometrics.agents.aggregator.manager.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "agent_registry")
public class AgentRegistry {
    @Id
    Long uid;

    @Column
    String caption;
}
