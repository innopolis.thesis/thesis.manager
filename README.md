### Instructions to add new agent:

<ol>
<li>add to migrations: <ol> 
<li>new agent type in resources/db/migrations/agents.sql</li>
<li>new migration for agent registry item  by following template specified in resources/db/migrations/log.yaml</li>
</ol></li>
<li>Create needed entities, but most important is to implement AgentService interface</li>
<li>In StartJob service, add new case in method getAgentService(AgentRegistry registry);</li>

</ol>

note: see example in gitlabV2 package




command to run service:

docker service create --name manager --publish published=8081,target=8081 --replicas 1 pryran/thesis.manager:latest

### DO NOT FORGOT TO UPDATE AGGREGATOR IP IN application.yml